package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseSingleLine(t *testing.T) {
	Flush()
	HookToken(TokLabel(TokLeftPar))
	HookToken(TokLabel(TokRightPar))
	HookToken(TokLabel(TokQuote))
	HookToken(TokLabel(TokIdentifier))
	HookToken(TokLabel(TokEof))
	HookToken(TokLabel("concat"))
	HookToken(TokLabel("string"))

	input := "(concat (concat \"a\") \"b\")"

	parser := NewParser()
	got, err := parser.Parse(input)
	assert.NoError(t, err)

	want := `(root:
 (concat:
  (concat:
   (string "a")
  )
  (string "b")
 )
)
`
	assert.Equal(t, want, got.String())
	Flush()
}

func TestParseMultiLine(t *testing.T) {
	Flush()
	HookToken(TokLabel(TokLeftPar))
	HookToken(TokLabel(TokRightPar))
	HookToken(TokLabel(TokQuote))
	HookToken(TokLabel(TokIdentifier))
	HookToken(TokLabel(TokEof))
	HookToken(TokLabel("concat"))
	HookToken(TokLabel("string"))

	input := `(concat (concat "x") "y")
(concat (concat "a") "b")`

	parser := NewParser()
	got, err := parser.Parse(input)
	assert.NoError(t, err)

	want := `(root:
 (concat:
  (concat:
   (string "x")
  )
  (string "y")
 )
 (concat:
  (concat:
   (string "a")
  )
  (string "b")
 )
)
`

	assert.Equal(t, want, got.String())
	Flush()
}

func TestEvaluateExpression(t *testing.T) {
	Flush()
	HookToken(TokLabel(TokLeftPar))
	HookToken(TokLabel(TokRightPar))
	HookToken(TokLabel(TokQuote))
	HookToken(TokLabel(TokIdentifier))
	HookToken(TokLabel(TokEof))
	HookToken(TokLabel(TokString))
	HookToken(TokLabel("concat"))
	parser := NewParser()

	t.Run("Valid utf8 input should be parsed", func(t *testing.T) {
		input := `(concat "hello " "세계")`

		got, err := parser.Parse(input)
		assert.NoError(t, err)
		want := `(root:
 (concat:
  (string "hello ")
  (string "세계")
 )
)
`
		assert.Equal(t, want, got.String())
		Flush()
	})

	t.Run("invalid utf8 input cannot be parsed", func(t *testing.T) {
		validUTF8 := `(concat "hello " "세계")`
		invalidUTF8 := string([]byte{0xff, 0xfe, 0xfd})
		validUTF8 += invalidUTF8
		_, err := parser.Parse(invalidUTF8)
		assert.EqualError(t, err, "can't parse non utf8 input")
		Flush()
	})
}
