package parser

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLexerSimple(t *testing.T) {
	Flush()
	var tokLeftPar = HookToken(TokLabel(TokLeftPar))
	var tokRightPar = HookToken(TokLabel(TokRightPar))
	HookToken(TokLabel(TokQuote))
	HookToken(TokLabel(TokIdentifier))
	HookToken(TokLabel(TokEof))
	HookToken(TokLabel("match"))
	HookToken(TokLabel("name"))
	HookToken(TokLabel("string"))

	lex := NewLexer("(desc)")
	identifier, _ := NewToken(TokLabel(TokIdentifier), "desc")

	want := []Tok{
		*tokLeftPar,
		identifier,
		*tokRightPar,
	}

	got := []Tok{}
	var tok Tok
	var err error

	for tok, err = lex.NextToken(); !tok.IsEof() && err == nil; tok, err = lex.NextToken() {
		got = append(got, tok)
	}

	if err != nil {
		t.Error(err.Error())
	}

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}

	Flush()
}

func TestLexerUnicodeLetter(t *testing.T) {
	cases := []struct {
		name  string
		input string
	}{
		{
			name:  "token with unicode at the beginning",
			input: "합add",
		},
		{
			name:  "token with unicode in the middle",
			input: "add합add",
		},
		{
			name:  "token with unicode at the end",
			input: "add합",
		},
		{
			name:  "token with single unicode",
			input: "합",
		},
		{
			name:  "token with multiple unicode",
			input: "합合",
		},
	}

	for _, tc := range cases {
		Flush()
		var tokLeftPar = HookToken(TokLabel(TokLeftPar))
		var tokRightPar = HookToken(TokLabel(TokRightPar))
		HookToken(TokLabel(TokIdentifier))
		HookToken(TokLabel(TokEof))

		t.Run(tc.name, func(t *testing.T) {
			lex := NewLexer(fmt.Sprintf("(%s)", tc.input))
			identifier, _ := NewToken(TokLabel(TokIdentifier), tc.input)

			want := []Tok{
				*tokLeftPar,
				identifier,
				*tokRightPar,
			}

			got := []Tok{}
			var tok Tok
			var err error

			for tok, err = lex.NextToken(); !tok.IsEof() && err == nil; tok, err = lex.NextToken() {
				got = append(got, tok)
			}

			if err != nil {
				t.Error(err.Error())
			}

			if !reflect.DeepEqual(want, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
			}
		})

		Flush()
	}
}

func TestLexerNonLetterUnicode(t *testing.T) {
	cases := []struct {
		name  string
		input string
	}{
		{
			name:  "number is not letter",
			input: "½",
		},
		{
			name:  "symbol is not letter",
			input: "→",
		},
	}

	for _, tc := range cases {
		Flush()
		HookToken(TokLabel(TokIdentifier))
		HookToken(TokLabel(TokEof))

		t.Run(tc.name, func(t *testing.T) {
			lex := NewLexer(tc.input)
			_, err := lex.NextToken()
			assert.Error(t, err, tc.name)
		})

		Flush()
	}
}

func TestLexerAdvanced(t *testing.T) {
	Flush()
	var tokLeftPar = HookToken(TokLabel(TokLeftPar))
	var tokRightPar = HookToken(TokLabel(TokRightPar))
	var tokMatch = HookToken(TokLabel("match"))
	var tokName = HookToken(TokLabel("name"))
	HookToken(TokLabel(TokQuote))
	HookToken(TokLabel(TokIdentifier))
	HookToken(TokLabel(TokEof))
	HookToken(TokLabel("string"))

	lex := NewLexer("(match (name X) \"(exec|match)\")")

	ident, _ := NewToken(TokLabel(TokIdentifier), "X")
	pat, _ := NewToken(TokLabel(TokString), "(exec|match)")

	want := []Tok{
		*tokLeftPar,
		*tokMatch,
		*tokLeftPar.Label.ToToken(),
		*tokName,
		ident,
		*tokRightPar,
		pat,
		*tokRightPar,
	}

	got := []Tok{}
	var tok Tok
	var err error

	for tok, err = lex.NextToken(); !tok.IsEof() && err == nil; tok, err = lex.NextToken() {
		got = append(got, tok)
	}

	if err != nil {
		t.Error(err.Error())
	}

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
	Flush()
}
